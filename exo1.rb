puts "Chaine 1"
string1 = gets.chomp

puts "Chaine 2"
string2 = gets.chomp

# string2 = gets
# string2.chomp!

if string1.length >= string2.length * 2
  puts "La premiere chaîne de caractere est deux fois plus grande que la deuxieme"
elsif string2.length >= string1.length * 2
  puts "La deuxieme chaîne de caractere est deux fois plus grande que la premiere"
else
  puts "Les deux chaines sont égaux"
end
